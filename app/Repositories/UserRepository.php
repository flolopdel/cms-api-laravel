<?php

namespace App\Repositories;

use InfyOm\Generator\Common\BaseRepository;
use App\Models\Role;
use app\Helpers\Common;
use Auth;
use App\Models\User;

/**
 * Class UserRepository
 * @package namespace App\Repositories;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email' => 'like',
    	'name' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * Create user
     **/
    public function create(array $attributes)
    {
        if(isset($attributes['password'])){
            $attributes['password'] = bcrypt($attributes['password']);
        }
        return parent::create($attributes);
    }
}
