<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use Auth;
use Hash;
use Validator;
use App\Exceptions\ApiAuthenticationCredentialsException;
use App\Exceptions\ApiAuthenticationJWTAuthException;
use App\Exceptions\ApiValidationException;
use App\Repositories\UserRepository;
use App\Http\Requests\API\CreateUserRequest;


class AuthenticateController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /**
     * Return a JWT
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // verify the credentials and create a token for the user
        try {
            // GET TOKEN AND SESSION
            if (!$token = JWTAuth::attempt($credentials)) {
                throw new ApiAuthenticationCredentialsException('Could not generate token for email: ' . $credentials['email'] . ' and pass: ' . $credentials['password']);
            }
        } catch(JWTException $e) {
            // something went wrong
            throw new ApiAuthenticationJWTAuthException($e->getMessage());
        }
        
        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }



    public function register(Request $request)
    {

        $input = $request->only('email', 'password', 'name', 'password_confirmation');

        $user_rules = User::$rules;
        $validator = Validator::make($input, $user_rules);

        if($validator->fails()){
            throw new ApiValidationException($validator->messages()->first());
        }
        
        $user = $this->userRepository->create($input);
        
        // if no errors are encountered we can return a JWT
        return response()->json($user->toArray(), 200);
    }
}