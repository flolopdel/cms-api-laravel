<?php

namespace App\Exceptions;

/**
 * Class ApiRegisterMatchUserException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiRegisterMatchUserException extends ApiException
{
	const API_REGISTER_MATCH_ERROR = 30;
	
	public function __construct($message){
		parent::__construct($message, self::API_REGISTER_MATCH_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_USER_MATCH);
		$this->setClientMessage(trans('register.match'));
	}
}
