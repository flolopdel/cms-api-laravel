<?php

namespace App\Exceptions;

/**
 * Class ApiAuthenticationTokenException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiAuthenticationTokenException extends ApiException
{
	const API_AUTHENTICATE_TOKEN_ERROR = 22;
	
	public function __construct($message){
		parent::__construct($message, self::API_AUTHENTICATE_TOKEN_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_UNAUTHORIZED);
		$this->setClientMessage(trans('auth.failed'));
	}
}
