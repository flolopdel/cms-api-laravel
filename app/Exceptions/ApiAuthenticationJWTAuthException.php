<?php

namespace App\Exceptions;

/**
 * Class ApiAuthenticationJWTAuthException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiAuthenticationJWTAuthException extends ApiException
{
	const API_AUTHENTICATE_JWT_ERROR = 23;
	
	public function __construct($message){
		parent::__construct($message, self::API_AUTHENTICATE_JWT_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_UNAUTHORIZED);
		$this->setClientMessage(trans('auth.uncaught'));
	}
}
