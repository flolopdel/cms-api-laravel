<?php

namespace App\Exceptions;

/**
 * Class ApiAuthenticationException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiAuthenticationException extends ApiException
{
	const API_AUTHENTICATE_ERROR = 20;
	
	public function __construct($message){
		parent::__construct($message, self::API_AUTHENTICATE_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_UNAUTHORIZED);
		$this->setClientMessage(trans('auth.uncaught'));
	}
}
