<?php

namespace App\Exceptions;

/**
 * Class ApiException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiException extends \Exception
{
	//Http Status Codes
	const HTTP_CODE_BAD_REQUEST = 400;
	const HTTP_CODE_UNAUTHORIZED = 401;
	const HTTP_CODE_FORBIDDEN = 403;
	const HTTP_CODE_NOT_FOUND = 404;
	const HTTP_CODE_USER_MATCH = 409;
	const HTTP_CODE_INTERNAL_SERVER_ERROR = 500;
	
	const API_INTERNAL_SERVER_ERROR = 10;
	
	/**
	 * Attributes
	 * @var unknown
	 */
	private $httpCode;
	
	private $clientMessage;
	
	/**
	 * @param $message [optional]
	 * @param $code [optional]
	 * @param $previous [optional]
	 */
	public function __construct ($message, $code = self::API_INTERNAL_SERVER_ERROR, $previous = null) {
		$this->setHttpCode(self::HTTP_CODE_INTERNAL_SERVER_ERROR);
		$this->setClientMessage(trans('validation.uncaught'));

 		parent::__construct($message, $code, $previous);
	}
	
	public function getHttpCode(){
		return $this->httpCode;
	}
	
	public function setHttpCode($httpCode){
		$this->httpCode = $httpCode;
	}
	
	public function getClientMessage(){
		return $this->clientMessage;
	}
	
	public function setClientMessage($message){
		$this->clientMessage = $message;
	}
	
}