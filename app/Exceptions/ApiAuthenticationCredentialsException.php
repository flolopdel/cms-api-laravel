<?php

namespace App\Exceptions;

/**
 * Class ApiAuthenticationCredentialsException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiAuthenticationCredentialsException extends ApiException
{
	const API_AUTHENTICATE_CREDENTIALS_ERROR = 21;
	
	public function __construct($message){
		parent::__construct($message, self::API_AUTHENTICATE_CREDENTIALS_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_UNAUTHORIZED);
		$this->setClientMessage(trans('auth.failed'));
	}
}
