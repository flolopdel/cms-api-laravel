<?php

namespace App\Exceptions;

/**
 * Class ApiValidationException
 * @package namespace App\Exceptions;
 * @author Ing. Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiValidationException extends ApiException
{
	const API_VALIDATION_ERROR = 40;
	
	public function __construct($message){

		parent::__construct($message, self::API_VALIDATION_ERROR);
		
		$this->setHttpCode(parent::HTTP_CODE_BAD_REQUEST);
		$this->setClientMessage($message);
	}
}
