<!-- 

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Angular-Laravel Authentication</title>
        <link rel="stylesheet" href="angular-app/node_modules/bootstrap/dist/css/bootstrap.css">
    </head>
    <body ng-app="authApp">
        
        <div class="container">
        	<div ui-view></div>
        </div>     
     
    </body>

    <script src="angular-app/node_modules/angular/angular.js"></script>
    <script src="angular-app/node_modules/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="angular-app/node_modules/satellizer/satellizer.js"></script>
 
    <script src="angular-app/scripts/app.js"></script>
    <script src="angular-app/scripts/authController.js"></script>
    <script src="angular-app/scripts/userController.js"></script>
</html>

-->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title>Laravel - Ionic</title>

    <link href="ionic/www/lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="ionic/www/css/style.css" rel="stylesheet">
    <link href="ionic/www/css/custom-app.css" rel="stylesheet">

    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="ionic/www/lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="ionic/www/js/app.js"></script>
    <script src="ionic/www/js/controllers.js"></script>
    <script src="ionic/www/js/services.js"></script>
  </head>

  <body ng-app="starter">
    <ion-nav-view></ion-nav-view>
  </body>
</html>