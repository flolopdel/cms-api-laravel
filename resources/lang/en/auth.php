<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'token' => 'Could not generate the access token',
    'uncaught' => 'An error occured whent trying to authenticate.',
    'token_missing' => 'Token not provided.',
    'token_expired' => 'Token expired.',
    'token_invalid' => 'Invalid token.'


];
