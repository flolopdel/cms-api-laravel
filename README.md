# CMS - LARAVEL
##### Cms con todo el contenido del bundle http://labs.infyom.com/laravelgenerator/

Instalación:

1. composer update
2. composer dump-autoload
3. php artisan vendor:publish
4. php artisan key:generate
5. php artisan cache:clear
6. php artisan infyom.publish:templates
7. php artisan infyom.publish:layout

8. mkdir -pv bootstrap/cache storage/framework/views storage/app storage/framework/sessions storage/framework/cache
9. chmod -R 0777 bootstrap/ storage/